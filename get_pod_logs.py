from kubernetes.client.rest import ApiException
from kubernetes import client, config


def main():
    try:
        config.load_incluster_config()
    except config.ConfigException:
        try:
            config.load_kube_config()
        except config.ConfigException:
            raise Exception("Could not load k8s python client")

    configuration = client.Configuration()

    print("Current host is %s" % configuration.host)

    v1 = client.CoreV1Api()

    podList = getPodList(v1)
    printPodLogs(v1, podList)


def getPodList(ApiObject):

    pods = ApiObject.list_pod_for_all_namespaces(watch=False)

    return pods


def printPodLogs(ApiObject, pod_list):
    try:
        for pod in pod_list.items:
            podLogs = ApiObject.read_namespaced_pod_log(
                name=pod.metadata.name, namespace=pod.metadata.namespace)
            print("*"*60)
            print("Printing POD %s logs" % pod.metadata.name)
            print("*"*60)
            print(podLogs)
            print("\n"*2)
    except ApiException as e:
        print('Found exception reading the logs')


if __name__ == '__main__':
    main()
