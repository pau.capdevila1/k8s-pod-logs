# k8s-pod-logs

exercise: write a script that gets the logs from the pods in a k8s cluster

1. execute: pip install -r requirements.txt in your shell
2. execute: python get_pod_logs.py 

Example output:

Current host is http://localhost
************************************************************
Printing POD hello-minikube-6ddfcc9757-wx869 logs
************************************************************
172.17.0.1 - - [15/Jul/2021:07:51:52 +0000] "GET / HTTP/1.1" 200 664 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
172.17.0.1 - - [15/Jul/2021:07:51:52 +0000] "GET /favicon.ico HTTP/1.1" 200 634 "http://192.168.49.2:30522/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
172.17.0.1 - - [15/Jul/2021:07:52:07 +0000] "GET / HTTP/1.1" 200 664 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
172.17.0.1 - - [15/Jul/2021:07:52:10 +0000] "GET / HTTP/1.1" 200 664 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
172.17.0.1 - - [15/Jul/2021:07:52:11 +0000] "GET / HTTP/1.1" 200 694 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
172.17.0.1 - - [15/Jul/2021:07:52:11 +0000] "GET / HTTP/1.1" 200 694 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"
172.17.0.1 - - [15/Jul/2021:07:52:11 +0000] "GET / HTTP/1.1" 200 694 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0"

Possible functional improvements:

1. Allow selection of cluster instance through command line arguments
2. Allow filtering by namespace
3. Allow filtering by pod name
4. Allow dynamic log updates (-f)

Possible programming improvements:

1. Add better error catching
2. Add testing
3. Improve usage os kubernetes APIs
4. Document code
